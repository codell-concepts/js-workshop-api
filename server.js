const express = require('express');
const logger = require('morgan');
const config = require('config');
const songs  = require('./songs');
const root = __dirname;

const app = express();
app.set('root', root);

if (config.debug === true) {
  app.use(logger('dev'));
}

app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).send({error: err});
  next();
});

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', songs);

app.use('/songs', songs);

app.listen(config.port, () => {
  console.log('listening on port ' + config.port);
});
