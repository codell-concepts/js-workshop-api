const express = require('express');
const router = express.Router();
const songs = [
  { id: 1, title: 'Paris', artist: 'Chainsmokers', genre: 'pop' },
  { id: 2, title: 'Sheena Is a Punk Rocker', artist: 'Ramones', genre: 'punk' },
  { id: 3, title: "So What'Cha Want", artist: 'Beastie Boys', genre: 'rap' },
  { id: 4, title: 'Vicarious', artist: 'Tool', genre: 'rock' },
  { id: 5, title: 'How You Remind Me', artist: 'Nickelback', genre: 'rock' },
  { id: 6, title: 'One Step Closer', artist: 'Linkin Park', genre: 'alternative' },
  { id: 7, title: 'Slow Cheetah', artist: 'Red Hot Chili Peppers', genre: 'alternative' },
  { id: 8, title: 'Tha Shiznit', artist: 'Snoop Dogg', genre: 'rap' }
];

router.get('/', (req, res) => {
  res.json(songs);
});

router.get('/genre/:genre', (req, res) => {
  res.json(songs.filter(song => song.genre === req.params.genre));
});

router.get('/artist/:artist', (req, res) => {
  res.json(songs.filter(song => song.artist === req.params.artist));
});

module.exports = router;
