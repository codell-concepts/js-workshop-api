## Boardwalk JavaScript Workshop API

Simple API for songs

[ ![Codeship Status for codell-concepts/js-workshop-api](https://app.codeship.com/projects/55541540-e126-0134-e660-72cc6e2df2e1/status?branch=master)](https://app.codeship.com/projects/205434)

> #### Developer Setup

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

``` bash
# Go to your projects directory
cd wherever-you-store-your-projects

# Clone this repository
git clone https://codell-concepts@bitbucket.org/codell-concepts/js-workshop-api.git

# Go into the repository
cd js-workshop-api

# install dependencies
npm install

```